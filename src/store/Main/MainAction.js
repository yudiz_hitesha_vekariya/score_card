import { FETCH_DATA } from "../action-types";

const fetchData = (data) => {
  return {
    type: FETCH_DATA,
    payload: data,
  };
};


export default {
  fetchData,
};
