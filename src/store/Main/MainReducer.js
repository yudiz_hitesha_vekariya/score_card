import { FETCH_DATA } from "../action-types";

const initialState = {
  mainData: {},
};

const main = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA:
      return {
        ...state,
        mainData: action.payload,
      };

    default:
      return state;
  }
};

export default main;
