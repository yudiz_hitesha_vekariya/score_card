import axios from "axios";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useQuery } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import { Helmet } from "react-helmet";

import "./App.scss";
import { Card } from "./components";
import allActions from "./store/allActions";


function App() {
 

  const dispatch = useDispatch();

 

  const { isLoading, data } = useQuery("score-board-data", () =>
    axios.get(
      "https://backend.sports.info/api/v1/nba/game/f34b1dfd-97fd-4942-9c14-05a05eeb5921/summary"
    )
  );

  useEffect(() => {
    dispatch(allActions.MainAction.fetchData(data?.data?.data));
  }, [data]);

  if (isLoading) {
    return (
      <div className="app">
        <h1>Loading...</h1>;
      </div>
    );
  }

  return (
    <>
      
        <div className="app">
          <Helmet>
            <meta charSet="utf-8" />
            <title>Score-Board</title>
            <link rel="canonical" href="http://localhost4200" />
          </Helmet>

          <Card />

          <ReactQueryDevtools position="bottom-right" />
        </div>
     

     

      
    </>
  );
}

export default App;
