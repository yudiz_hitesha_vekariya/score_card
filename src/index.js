import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { QueryClient, QueryClientProvider } from "react-query";
import * as Sentry from "@sentry/react";
import { BrowserTracing } from "@sentry/tracing";

import "./index.scss";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import store from "./store";

const queryClient = new QueryClient();

Sentry.init({
  dsn: "https://b346eafb88cf450081597374581d7904@o1203778.ingest.sentry.io/6346387",
  integrations: [new BrowserTracing()],
  tracesSampleRate: 1.0,
});

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  </Provider>
);

reportWebVitals();
